import os
import random
import pyglet
from RPi import GPIO
from datetime import datetime

from gpio.led import Led, LedController

from config import basedir, led_pins, sensor, caps_input_sound

def select_random_file(folder_path: str) -> str:
    """ Method that select a random file on the specified folder """
    files: list = os.listdir(folder_path)
    index: int = random.randint(0, len(files))
    return files[index]


def play_sound(sound_path: str):
    """ Method that play a sound on the speakers """
    audio = pyglet.media.load(sound_path)
    audio.play()


if __name__ == "__main__":
    """ Main method that call everything """
    # GPIO settings
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # Setup GPIO
    for p in led_pins:
        GPIO.setup(p, GPIO.OUT)
    GPIO.setup(sensor, GPIO.IN)

    led_controller: LedController = LedController()

    # Main loop
    while True:
        # If a caps is detected by the ir sensor, play a sound and a random led animation
        if GPIO.input(sensor):
            play_sound(os.path.joint(basedir, caps_input_sound))
            led_controller.play_animation(select_random_file(os.path.join(basedir, "anim/")))

        # If it's 19:19, play a random sound and a random led animation
        if datetime.now().hour == 19 and datetime.now().minute == 19:
            play_sound(select_random_file(os.path.join(basedir, "/sound")))
            led_controller.play_animation(select_random_file(os.path.join(basedir, "anim/")))