from RPi import GPIO
import time

class Led:
    """ Class representing a Led """
    def __init__(self, pin: int):
        """ Create a new RGB led object to encapsulate gpio rgb led data """
        # Init GPIO pins
        GPIO.setup(pin, GPIO.OUT)
        # PWN frequency and setting up pin
        self.FREQ = 75
        self.PIN = GPIO.PWM(pin, 255)

    def switch_on(self):
        """ Method to switch on a led """
        self.PIN.start(0)

    def set_intensity(self, value: int):
        """ Method to change the intensity of the led """
        self.PIN.ChangeDutyCycle(value)
        time.sleep(0.05)

    def switch_off(self):
        """ Method to switch off a led by closing its PWM """
        self.PIN.stop()

    def get_pin(self) -> int:
        """ Method that return the led's pin """
        return self.PIN


class LedList():
    """ Class representing a led list with custom method to manipulate it """
    def __init__(self):
        leds_list: list[Led] = []

    def add_led(self, led: Led):
        """ Method to add a led to the list """
        self.leds_list.append(led)

    def find_led(self, pin: int) -> Led:
        """ Method that find a led by pin """
        for l in self.leds_list:
            if l.get_pin() == pin:
                return l

class LedController:
    """ Class representing a Led controller that can play animation """
    def play_animation(animation_path: str):
        """ Method that plays the given animation """
        # Create some variable
        loop: int = 0
        leds: LedList = LedList()
        # Opening animation file
        anim_file = open(animation_path, 'r')
        # Getting all lines of the script
        instructions: list[str] = anim_file.readlines()
        # Checking if important variables are set on the begining of the script
        if "#LOOP" in instructions[0] and "#PINS" in instructions[1]:
            # Getting the number of loop to do
            loop: int = int(instructions[0].split(':')[1])
            # Getting the led pins to initialize Led objects
            leds_pin: list[int] = [x for x in int(instructions[1].split(":")[1].split(","))]
            # Creating led objects
            for p in leds_pin:
                leds.add_led(Led(p))
            # Interpreting each lines of the script 'loop' time
            for i in range(loop):
                for instruct in instructions[2:]:
                    sequence: list[str] = instruct[1:-1].split(",")
                    # Interpreting sequence instruction
                    for s in sequence:
                        if s[0] == '%':
                            # Changing a led intensity
                            intensity, pin = s[1:].split(":")
                            # If both values are correct integer
                            if intensity.isnumeric() and pin.isnumeric():
                                # Check if intensity has a value in [1,99]
                                if 0 < intensity and intensity < 100:
                                    leds.find_led(int(pin)).set_intensity(int(intensity))
                        elif s[0] == '~':
                            # Set a timer
                            delay: str = s[1:]
                            # Checking if timer is numeric
                            if delay.isnumeric():
                                # Set delay
                                time.sleep(int(delay))
                        elif s[0] == '!':
                            # Switch off a led
                            pin: str = s[1:]
                            # Cheking if pin is numeric
                            if pin.isnumeric():
                                # Switch off the specific led
                                leds.find_led(int(pin)).switch_off()
                        elif s[0].isnumeric():
                            # Switch on a led
                            pin: str = s[1:]
                            # Cheking if pin is numeric
                            if pin.isnumeric():
                                # Switch off the specific led
                                leds.find_led(int(pin)).switch_on()     