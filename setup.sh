virtualenv -p python3.9 venv
source venv/bin/activate
echo 'source venv/bin/activate' >> .envrc
direnv allow
pip install -r requirement.txt